package projetcvmplayer;

import java.awt.Dimension;

import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class FramePlayer extends JFrame {
    private JLabel labelNomChanson = new JLabel();
    private JProgressBar progressChanson = new JProgressBar();
    private JSlider sliderSeek = new JSlider();
    private JButton buttonPlay = new JButton();
    private JButton buttonPause = new JButton();
    private JButton buttonStop = new JButton();

    private Timer chrono;
    private int temps;
    private int tempsParcouru;
    private boolean timerPause;
    private WavInfo info;
    private WavDiffuseur2 diffuseur;
    private File chanson;
    private Playlist playlist;
    private Ecouteur ec;
    private JMenuBar jMenuBar1 = new JMenuBar();
    private JMenu menuFichier = new JMenu("Fichier");
    private JMenuItem itemDossier = new JMenuItem("Ouvrir Dossier");
    private JMenuItem itemPlaylist = new JMenuItem("Ouvrir Playlist");
    private JMenuItem itemFichier = new JMenuItem("Ouvrir Fichier");
    final JFileChooser fc = new JFileChooser();
    private JLabel labelTemps = new JLabel();
    private JButton buttonNext = new JButton();
    private JButton buttonPrev = new JButton();
    private int nbChansons;
    private int numChanson;
    private DefaultTableModel modeleTable = new DefaultTableModel();
    private JTable tableDeListe = new JTable(modeleTable);
    private Vector<Playlist> listeDeListe = new Vector();
    private JButton buttonNouvelleListe = new JButton();
    private JComboBox comboListe = new JComboBox();
    private JButton buttonAjoutChanson = new JButton();
    private JButton buttonSaveListe = new JButton();

    public FramePlayer() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.getContentPane().setLayout(null);
        this.setSize(new Dimension(564, 584));
        this.setJMenuBar(jMenuBar1);
        this.setTitle("MP3 Player");
        labelNomChanson.setText("Chanson");
        labelNomChanson.setBounds(new Rectangle(115, 10, 325, 75));
        progressChanson.setBounds(new Rectangle(5, 495, 555, 25));
        sliderSeek.setBounds(new Rectangle(60, 195, 445, 35));
        buttonPlay.setText("Play");
        buttonPlay.setBounds(new Rectangle(60, 110, 85, 60));
        buttonPause.setText("Pause");
        buttonPause.setBounds(new Rectangle(170, 110, 80, 60));
        buttonStop.setText("Stop");
        buttonStop.setBounds(new Rectangle(265, 105, 80, 70));

        this.getContentPane().add(buttonSaveListe, null);
        this.getContentPane().add(buttonAjoutChanson, null);
        this.getContentPane().add(comboListe, null);
        this.getContentPane().add(buttonNouvelleListe, null);
        this.getContentPane().add(tableDeListe, null);
        this.getContentPane().add(buttonPrev, null);
        this.getContentPane().add(buttonNext, null);
        this.getContentPane().add(labelTemps, null);
        this.getContentPane().add(buttonStop, null);
        this.getContentPane().add(buttonPause, null);
        this.getContentPane().add(buttonPlay, null);
        this.getContentPane().add(sliderSeek, null);
        this.getContentPane().add(progressChanson, null);
        this.getContentPane().add(labelNomChanson, null);

        menuFichier.add(itemFichier);
        menuFichier.add(itemDossier);
        menuFichier.add(itemPlaylist);
        jMenuBar1.add(menuFichier);

        ec = new Ecouteur();
        buttonPlay.addActionListener(ec);
        buttonPause.addActionListener(ec);
        buttonNext.addActionListener(ec);
        buttonPrev.addActionListener(ec);
        buttonNouvelleListe.addActionListener(ec);
        buttonAjoutChanson.addActionListener(ec);
        buttonSaveListe.addActionListener(ec);
        comboListe.addActionListener(ec);
        itemFichier.addActionListener(ec);
        itemDossier.addActionListener(ec);
        itemPlaylist.addActionListener(ec);


        labelTemps.setText("0:0");
        labelTemps.setBounds(new Rectangle(425, 240, 100, 25));
        buttonNext.setText("Next");
        buttonNext.setBounds(new Rectangle(375, 100, 81, 22));
        buttonPrev.setText("Previous");
        buttonPrev.setBounds(new Rectangle(375, 140, 81, 22));
        tableDeListe.setBounds(new Rectangle(220, 280, 335, 205));
        buttonNouvelleListe.setText("Nouvelle Liste");
        buttonNouvelleListe.setBounds(new Rectangle(35, 320, 120, 30));
        comboListe.setBounds(new Rectangle(30, 265, 130, 30));
        buttonAjoutChanson.setText("Ajout chanson");
        buttonAjoutChanson.setBounds(new Rectangle(35, 370, 120, 25));
        buttonSaveListe.setText("Sauvegarde Liste");
        buttonSaveListe.setBounds(new Rectangle(15, 410, 160, 30));
        buttonStop.addActionListener(ec);
        progressChanson.setIndeterminate(true);
        timerPause = false;

        numChanson = 0;
        modeleTable.addColumn("Chanson");
    }

    public void ouvreDossier() {
        int returnVal = fc.showOpenDialog(FramePlayer.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println(fc.getCurrentDirectory());
            playlist = new Playlist(fc.getCurrentDirectory());
            info = new WavInfo(playlist.getListe().elementAt(numChanson));

            if (diffuseur != null)
                diffuseur.setStatut(WavDiffuseur2.STOP);
            diffuseur = new WavDiffuseur2(info);
            lanceChanson();
            labelNomChanson.setText(info.getFichier().getName());
            System.out.println("Ouverture de la chanson");

            nbChansons = playlist.getListe().size();
        } else {
            System.err.println("Peut pas ouvrir le fichier");
        }
    }

    public void ouvreChanson() {
        int returnVal = fc.showOpenDialog(FramePlayer.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            chanson = fc.getSelectedFile();
            info = new WavInfo(chanson);

            if (diffuseur != null)
                diffuseur.setStatut(WavDiffuseur2.STOP);
            diffuseur = new WavDiffuseur2(info);
            lanceChanson();
            labelNomChanson.setText(info.getFichier().getName());
            System.out.println("Ouverture de la chanson");
        } else {
            System.err.println("Peut pas ouvrir le fichier");
        }
    }

    public void ouvrePlaylist() {
        int returnVal = fc.showOpenDialog(FramePlayer.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            playlist = new Playlist(fc.getSelectedFile());
            info = new WavInfo(playlist.getListe().elementAt(numChanson));

            if (diffuseur != null)
                diffuseur.setStatut(WavDiffuseur2.STOP);
            diffuseur = new WavDiffuseur2(info);
            lanceChanson();
            labelNomChanson.setText(info.getFichier().getName());
            System.out.println("Ouverture de la chanson");

            remplitTable();
            listeDeListe.add(playlist);
            updateComboBox();
            nbChansons = playlist.getListe().size();
        } else {
            System.err.println("Peut pas ouvrir le fichier");
        }
    }

    public void arreteChanson() {
        diffuseur.setStatut(WavDiffuseur2.STOP);
        stopTimer();
    }

    public void lanceTimer() {
        temps = 0;
        chrono = new Timer(1000, ec);
        sliderSeek.setMaximum(info.getNbSec());
        System.out.println(sliderSeek.getMaximum());
        System.out.println(info.getNbSec());
        chrono.start();
    }

    public void relanceChanson() {
        chrono = new Timer(1000, ec);
        chrono.start();
        diffuseur.setStatut(WavDiffuseur2.PLAY);
    }

    public void stopTimer() {
        chrono.stop();
        sliderSeek.setValue(0);
    }

    public void pauseTimer() {
        tempsParcouru = sliderSeek.getValue();
        timerPause = true;
        chrono.stop();
    }

    public void lanceChanson() {
        try {
            info = new WavInfo(info.getFichier());
            diffuseur = new WavDiffuseur2(info);
            lanceTimer();
            diffuseur.start();
        } catch (NullPointerException npe) {
            ;
        }
    }

    public void nextChanson() {
        arreteChanson();
        try {
            info = new WavInfo(playlist.getListe().elementAt(++numChanson));
            lanceChanson();
        } catch (Exception e) {
            numChanson--;
        }
    }

    public void prevChanson() {
        arreteChanson();
        try {
            info = new WavInfo(playlist.getListe().elementAt(--numChanson));
            lanceChanson();
        } catch (Exception e) {
            numChanson++;
        }
    }

    public void pauseChanson() {
        pauseTimer();
        diffuseur.setStatut(WavDiffuseur2.PAUSE);
    }

    public void videTable() {
        modeleTable.setRowCount(0);
        tableDeListe.repaint();
    }

    public void remplitTable() {
        videTable();
        for (File f : playlist.getListe()) {
            modeleTable.addRow(new Object[] { f.getName() });
        }
    }

    public void updateComboBox() {
        comboListe.addItem(listeDeListe.lastElement().getPlaylist().getName());
    }

    public void nouvellePlaylist() {
        String nom =
            JOptionPane.showInputDialog(this, "Nom de la nouvelle liste:");
        if (!listeDeListe.contains(new File(nom))) {
            System.out.println(nom + " added");
            playlist = new Playlist(new File(nom));
            listeDeListe.add(playlist);
            updateComboBox();
        } else {
            JOptionPane.showMessageDialog(this,
                                          "Vous ne pouvez pas avoir une playlist du nom de " +
                                          nom +
                                          " parce qu'elle existe deja dans le systeme.");
            //Excel fait pareil..
        }
    }

    public void ajouterChanson(String liste) {
        int pos = 0;
        for (int i = 0; i < listeDeListe.size(); i++) {
            if (listeDeListe.elementAt(i).getPlaylist().getName() == liste) {
                pos = i;
                break;
            }
        }

        int returnVal = fc.showOpenDialog(FramePlayer.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            if (playlist != null) {
                listeDeListe.elementAt(pos).addFichier(f);
                info = new WavInfo(f);
            } else {
                playlist = new Playlist(f);
                listeDeListe.add(pos, playlist);
                info = new WavInfo(f);
            }
            remplitTable();
        } else {
            System.err.println("Peut pas ouvrir le fichier");
        }
    }

    public void changerListe(String liste) {
        int pos = 0;
        for (int i = 0; i < listeDeListe.size(); i++) {
            if (listeDeListe.elementAt(i).getPlaylist().getName() == liste) {
                pos = i;
                break;
            }
        }

        playlist = listeDeListe.elementAt(pos);
        remplitTable();
        try {
            info = new WavInfo(playlist.getListe().elementAt(0));
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            System.out.println("Probleme de oob");
        }
    }

    public String formatTemps(int sec) {
        int min = sec / 60;
        sec = sec % 60;
        String ret =
            String.valueOf(min) + ":" + ((sec < 10) ? "0" + String.valueOf(sec) :
                                         String.valueOf(sec));

        min = info.getNbSec() / 60;
        sec = info.getNbSec() % 60;
        ret +=
"/" + String.valueOf(min) + ":" + ((sec < 10) ? "0" + String.valueOf(sec) :
                                   String.valueOf(sec));
        return ret;
    }

    protected class Ecouteur implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == buttonPlay) {
                if (diffuseur == null || !diffuseur.isAlive())
                    lanceChanson();
                else
                    relanceChanson();
            } else if (e.getSource() == buttonPause)
                pauseChanson();
            else if (e.getSource() == buttonStop)
                arreteChanson();
            else if (e.getSource() == itemFichier) {
                ouvreChanson();
            } else if (e.getSource() == itemDossier) {
                ouvreDossier();
            } else if (e.getSource() == itemPlaylist) {
                ouvrePlaylist();
            } else if (e.getSource() == chrono) {
                sliderSeek.setValue(temps++);
                System.out.println(sliderSeek.getValue());
                labelTemps.setText(formatTemps(temps));
            } else if (e.getSource() == buttonPrev) {
                prevChanson();
            } else if (e.getSource() == buttonNext) {
                nextChanson();
            } else if (e.getSource() == buttonNouvelleListe) {
                nouvellePlaylist();
            } else if (e.getSource() == buttonAjoutChanson) {
                ajouterChanson((String)comboListe.getSelectedItem());
            } else if (e.getSource() == comboListe) {
                changerListe((String)comboListe.getSelectedItem());
            } else if (e.getSource() == buttonSaveListe) {
                System.out.println("SAVE");
                playlist.sauvegarde();
            }
        }
    }

    public static void main(String[] arg) {
        FramePlayer f = new FramePlayer();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

