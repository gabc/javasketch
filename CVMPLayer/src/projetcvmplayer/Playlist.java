package projetcvmplayer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

public class Playlist {
    private File present;
    private File playlist;
    private Vector<File> liste;

    public Playlist(File chemin) {
        liste = new Vector();
        playlist = chemin;
        if (chemin.exists())
            if (chemin.isFile()) {
                litFichier(chemin);
            } else {
                litDossier(chemin);
            }
        else
            creeNouvelleListe(chemin);
    }

    public void creeNouvelleListe(File chemin) {
        BufferedWriter bw = null;
        String s = "";
        try {
            bw = new BufferedWriter(new FileWriter(chemin));
            bw.write("");
            playlist = chemin;
            liste = new Vector();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void litFichier(File fichier) {
        BufferedReader br = null;
        String s = "";
        try {
            br = new BufferedReader(new FileReader(fichier));
            while ((s = br.readLine()) != null) {
                liste.add(new File(s));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void litDossier(File dossier) {
        File[] fichiers = dossier.listFiles();
        for (File f : fichiers) {
            liste.add(f);
        }
    }

    public void sauvegarde() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(playlist));
            for (File f : liste) {
                bw.write(f.getAbsolutePath());
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addFichier(File fichier) {
        liste.add(fichier);
    }

    public void setPresent(File present) {
        this.present = present;
    }

    public File getPresent() {
        return present;
    }

    public void setPlaylist(File playlist) {
        this.playlist = playlist;
    }

    public File getPlaylist() {
        return playlist;
    }

    public void setListe(Vector<File> liste) {
        this.liste = liste;
    }

    public Vector<File> getListe() {
        return liste;
    }
}
