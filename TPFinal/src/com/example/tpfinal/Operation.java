package com.example.tpfinal;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Operation {
	private DatabaseHelper dbhelp;
	private SQLiteDatabase db;
	private static int routineCount = 0;

	public Operation(Context c){
		dbhelp = new DatabaseHelper(c);
	}

	public void ouvreBD(){
		db = dbhelp.getWritableDatabase();
	}

	public void fermeBD(){
		db.close();
	}

	public ArrayList<Hashtable<String,String>> trouveExo(){
		ArrayList<Hashtable<String,String>> liste = new ArrayList<Hashtable<String,String>>();
		Cursor c = db.rawQuery("SELECT * FROM exercice", null);
		c.moveToFirst();

		while(!c.isAfterLast()){
			Hashtable<String,String> h = new Hashtable<String,String>();
			h.put("id", String.valueOf(c.getInt(0)));
			h.put("nom", c.getString(1));
			h.put("muscle", c.getString(3));
			h.put("image", String.valueOf(c.getInt(2)));
			c.moveToNext();
			liste.add(h);
		}
		return liste;	
	}

	public Hashtable<String, String> getExobyId(int id){
		Hashtable<String, String> h = new Hashtable<String, String>();

		Cursor c = db.rawQuery("SELECT * FROM exercice where id = ", new String[]{String.valueOf(id)});
		try{
			c.moveToFirst();
			h.put("nom", c.getString(1));
			h.put("muscle", c.getString(3));
			h.put("image", String.valueOf(c.getInt(2)));
		}catch(Exception e){
			Log.i("Mine","Vide?");
		}
		return h;
	}

	public void saveRoutine(ArrayList<Hashtable<String,String>> liste){
		for(Hashtable<String, String> h: liste){
			Routine r = new Routine(routineCount, Integer.valueOf(h.get("id")));
			dbhelp.ajouterRoutine(r, db);
		}
		// Parce que je veux que les routines aient le meme id s'il sont le meme.
		routineCount++;
	}

	public ArrayList<Hashtable<String,String>> getRoutineById(int id){
		ArrayList<Hashtable<String,String>> arr = new ArrayList<Hashtable<String,String>>();

		Cursor c = db.rawQuery("SELECT * FROM routine WHERE routineId = ?", 
				new String[]{String.valueOf(id)});
		c.moveToFirst();

		while(!c.isAfterLast()){
			Hashtable<String,String> h = new Hashtable<String,String>();
			h.put("routineId", String.valueOf(c.getInt(1)));
			h.put("exercice", String.valueOf(c.getInt(2)));
			c.moveToNext();
			arr.add(h);
		}	
		return arr;
	}

	public void saveResultat(Hashtable<String,String> h){
		Performance p = new Performance(Integer.valueOf(h.get("exercice")), Integer.valueOf(h.get("repetition")), 
				Integer.valueOf(h.get("sequence")), Integer.valueOf(h.get("duree")), 
				h.get("commentaire"), Integer.valueOf(h.get("poid")));
		dbhelp.ajouterPerformance(p, db);
	}

	public Hashtable<String, String> getResult(int exo){
		Hashtable<String, String> h = new Hashtable<String, String>();
		Cursor c = db.rawQuery("SELECT * FROM performance WHERE exercice = ?", 
				new String[]{String.valueOf(exo)});	
		try{ 
			c.moveToFirst();
			h.put("exercice", String.valueOf(c.getInt(0)));
			h.put("repetition", String.valueOf(c.getInt(1)));
			h.put("sequence", String.valueOf(c.getInt(2)));
			h.put("duree", String.valueOf(c.getInt(3)));
			h.put("commentaire", c.getString(4));
			h.put("poid", String.valueOf(c.getInt(5)));
		}catch(Exception e){ // Oops I did it again.
			h.put("exercice", String.valueOf(0));
			h.put("repetition", String.valueOf(0));
			h.put("sequence", String.valueOf(0));
			h.put("duree", String.valueOf(0));
			h.put("commentaire", "");
			h.put("poid", String.valueOf(0));
		}
		return h;
	}
}

