package com.example.tpfinal;

import android.os.Bundle;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button perf;
	private Button nroutine;
	private Button goroutine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		perf = (Button)findViewById(R.id.bperf);
		nroutine = (Button)findViewById(R.id.nroutine);
		goroutine = (Button)findViewById(R.id.goroutine);
		Ecouteur ec = new Ecouteur();
		perf.setOnClickListener(ec);
		nroutine.setOnClickListener(ec);
		goroutine.setOnClickListener(ec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected class Ecouteur implements OnClickListener{
		public void onClick(View v) {
			if(v.getId() == R.id.bperf){
				Intent t = new Intent(MainActivity.this, ListeExercices.class);
				t.putExtra("INTERACTIVE", 0);
				startActivity(t);
			}else if(v.getId() == R.id.nroutine){
				startActivity(new Intent(MainActivity.this, NouvelleRoutine.class));
			}else if(v.getId() == R.id.goroutine){
				startActivity(new Intent(MainActivity.this, GoRoutine.class));
			}
		}
	}
}
