package com.example.tpfinal;

import java.util.ArrayList;
import java.util.Hashtable;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class NouvelleRoutine extends Activity {
	private EditText nom;
	private ListView liste;
	private ArrayList<Hashtable<String,String>> exos;
	private Button nouvel;
	private Button save;
	private Operation oper;
	private ArrayList<Hashtable<String,String>> listeExercices;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nouvelle_routine);
		nom = (EditText)findViewById(R.id.nomRoutine);
		liste = (ListView)findViewById(R.id.listeRoutine);
		nouvel = (Button)findViewById(R.id.addExo);
		oper = new Operation(this);
		exos = new ArrayList<Hashtable<String,String>>();
		save = (Button)findViewById(R.id.saveroutine);

		oper.ouvreBD();
		listeExercices = oper.trouveExo();
		oper.fermeBD();

		SimpleAdapter soa = new SimpleAdapter(this, exos, R.layout.exolayout,
				new String[]{"nom", "muscle", "image"}, 
				new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
		liste.setAdapter(soa);
		Ecouteur ec = new Ecouteur();
		nouvel.setOnClickListener(ec);
		save.setOnClickListener(ec);
		liste.setOnItemLongClickListener(ec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nouvelle_routine, menu);
		return true;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data){  
		super.onActivityResult(requestCode, resultCode, data); 
		if(resultCode == RESULT_OK && data != null)
			if(requestCode == 1){  
				//int idexo = data.getIntExtra("exercice", 0);
				Bundle extra = data.getExtras();
				Hashtable<String, String> result = getExoByName(extra.getString("exercice"));
				exos.add(result);

				SimpleAdapter soa = new SimpleAdapter(this, exos, R.layout.exolayout,
						new String[]{"nom", "muscle", "image"}, 
						new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
				liste.setAdapter(soa);
			}
	}  

	protected class Ecouteur implements OnClickListener, OnItemLongClickListener{
		@Override
		public void onClick(View v) {
			if(v.getId() == R.id.addExo){
				Intent t = new Intent(NouvelleRoutine.this, ListeExercices.class);
				t.putExtra("INTERACTIVE", 1);
				startActivityForResult(t, 1);
			}else if(v.getId() == R.id.saveroutine){
				oper.ouvreBD();
				oper.saveRoutine(exos);
				oper.fermeBD();
				finish();
			}
		}
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			// TODO Auto-generated method stub
			// Si on clique longtemps, on le retire de la liste.
			exos.remove(arg2);
			SimpleAdapter soa = new SimpleAdapter(NouvelleRoutine.this, exos, R.layout.exolayout,
					new String[]{"nom", "muscle", "image"}, 
					new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
			liste.setAdapter(soa);
			return false;
		}

	}
	// Retourne l'exercice par nom.
	private Hashtable<String, String> getExoByName(String name){
		Hashtable<String, String> h = new Hashtable<String, String>();

		for(Hashtable<String, String> i : listeExercices){
			if(i.get("nom").equals(name))
				return i;
		}
		return null;
	}
}
