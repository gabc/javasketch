package com.example.tpfinal;

public class Exercice {
	private String nom;
	private int image;
	private String muscle;
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public String getMuscle() {
		return muscle;
	}

	public void setMuscle(String muscle) {
		this.muscle = muscle;
	}

	public Exercice(String nom, String muscle, int image){
		this.nom = nom;
		this.image = image;
		this.muscle = muscle;
	}
}
