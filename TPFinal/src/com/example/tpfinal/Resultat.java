package com.example.tpfinal;

import java.util.Hashtable;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.NumberPicker;

public class Resultat extends Activity {
	private NumberPicker seq;
	private NumberPicker duree;
	private NumberPicker repet;
	private NumberPicker poids;
	private Chronometer chrono;
	private Button save;
	private Button quit;
	private Operation oper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultat);
		oper = new Operation(this);
		oper.ouvreBD();
		Hashtable<String, String> h = oper.getResult(getIntent().getIntExtra("exercice", -1));
		oper.fermeBD();
		
		seq = (NumberPicker)findViewById(R.id.nPickSeq);
		seq.setMaxValue(40);
		seq.setMinValue(0);
		seq.setValue(Integer.valueOf(h.get("sequence")));
		duree = (NumberPicker)findViewById(R.id.nPickDuree2);
		duree.setMaxValue(40);
		duree.setMinValue(0);
		duree.setValue(Integer.valueOf(h.get("duree")));
		repet = (NumberPicker)findViewById(R.id.nPickRep);
		repet.setMaxValue(40);
		repet.setMinValue(0);
		repet.setValue(Integer.valueOf(h.get("repetition")));
		poids = (NumberPicker)findViewById(R.id.nPickPoids);
		poids.setMaxValue(40);
		poids.setMinValue(0);
		poids.setValue(Integer.valueOf(h.get("poid")));
		chrono = (Chronometer)findViewById(R.id.chrono);
		save = (Button)findViewById(R.id.btnsave);
		quit = (Button)findViewById(R.id.btnquit);

		Ecouteur ec = new Ecouteur();
		save.setOnClickListener(ec);
		quit.setOnClickListener(ec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resultat, menu);
		return true;
	}

	protected class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			if(v.getId() == R.id.btnsave){
				Hashtable<String, String> temp = new Hashtable<String, String>();
				temp.put("exercice", String.valueOf(getIntent().getIntExtra("exercice", -1)));
				temp.put("sequence", String.valueOf(seq.getValue()));
				temp.put("repetition", String.valueOf(repet.getValue()));
				temp.put("duree", String.valueOf(duree.getValue()));
				temp.put("poid", String.valueOf(poids.getValue()));
				temp.put("commentaire", "");
				
				oper.ouvreBD();
				oper.saveResultat(temp);
				oper.fermeBD();
			}else if(v.getId() == R.id.btnquit){
				finish();
			}
		}
	}
}
