package com.example.tpfinal;

public class Performance {
	private int exercice;
	private int repetition;
	private int sequence;
	private int duree;
	private int poid;
	private String commentaire;
	
	public Performance(int exercice, int repetition, int sequence, int duree, String commentaire, int poid){
		this.exercice = exercice;
		this.repetition = repetition;
		this.sequence = sequence;
		this.duree = duree;
		this.poid = poid;
		this.commentaire = commentaire;
	}

	public int getExercice() {
		return exercice;
	}

	public void setExercice(int exercice) {
		this.exercice = exercice;
	}

	public int getRepetition() {
		return repetition;
	}

	public void setRepetition(int repetition) {
		this.repetition = repetition;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public int getPoid() {
		return poid;
	}

	public void setPoid(int poid) {
		this.poid = poid;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
}