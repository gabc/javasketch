package com.example.tpfinal;

import java.util.ArrayList;
import java.util.Hashtable;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @author 1136655
 *
 */
public class GoRoutine extends Activity {
	private ListView liste;
	private ArrayList<Hashtable<String,String>> routine;
	private Button btngo;
	private NumberPicker num;
	private Operation oper;
	private ArrayList<Hashtable<String,String>> listeExercices;
	private ArrayList<Hashtable<String,String>> listeRoutine;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_go_routine);
		liste = (ListView)findViewById(R.id.listeDeRoutine);
		btngo = (Button)findViewById(R.id.btngoroutine);
		num = (NumberPicker)findViewById(R.id.routinepicker);
		oper = new Operation(this);
		
		num.setMinValue(0);
		num.setMaxValue(150); // 150 c'est clairement assez.
		
		oper.ouvreBD();
		listeExercices = oper.trouveExo();
		oper.fermeBD();
		
		routine = new ArrayList<Hashtable<String,String>>();
		listeRoutine = new ArrayList<Hashtable<String,String>>();
		
		SimpleAdapter soa = new SimpleAdapter(this, listeRoutine, R.layout.exolayout,
				new String[]{"nom", "muscle", "image"}, 
				new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
		liste.setAdapter(soa);
		
		Ecouteur ec = new Ecouteur();
		btngo.setOnClickListener(ec);
		liste.setOnItemClickListener(ec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.go_routine, menu);
		return true;
	}
	
	protected class Ecouteur implements OnClickListener, OnItemClickListener{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			oper.ouvreBD();
			routine = oper.getRoutineById(num.getValue());
			oper.fermeBD();
			listeRoutine = getListeRoutine(routine);
			
			SimpleAdapter soa = new SimpleAdapter(GoRoutine.this, listeRoutine, R.layout.exolayout,
					new String[]{"nom", "muscle", "image"}, 
					new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
			liste.setAdapter(soa);
		}

		@Override
		public void onItemClick(AdapterView<?> va, View v, int index,
				long autre) {
			Intent i = new Intent(GoRoutine.this, Resultat.class);
			i.putExtra("exercice", listeExercices.get(index));
			startActivity(i);
		}
	}

	// Pour avoir la liste des exercices dans la routine.
	public ArrayList<Hashtable<String,String>> getListeRoutine(ArrayList<Hashtable<String,String>> lr){
		ArrayList<Hashtable<String,String>> h = new ArrayList<Hashtable<String,String>>();
		for(Hashtable<String,String> i : lr){
			for(Hashtable<String,String> e : listeExercices)
				if(i.get("exercice").equals(e.get("id"))){
					h.add(e);
					break;
				}
		}
		return h;
	}
}
