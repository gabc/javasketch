package com.example.tpfinal;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{

	public DatabaseHelper(Context context){
		super(context, "db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE exercice (_id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, image NUMBER, muscle TEXT);");
		ajouterExercice(new Exercice("Chin up", "Trap�zoidaux", R.drawable.chin_up), db);
		ajouterExercice(new Exercice("Course", "Cardio", R.drawable.course), db);
		ajouterExercice(new Exercice("D�velopp�-couch�", "Pectoraux", R.drawable.developpe_couche), db);
		ajouterExercice(new Exercice("Crunch", "Abdominaux", R.drawable.crunch), db);
		ajouterExercice(new Exercice("Dip", "Triceps", R.drawable.dip), db);
		ajouterExercice(new Exercice("�l�vation", "Delto�des", R.drawable.elevation), db);
		ajouterExercice(new Exercice("Elliptique", "Cardio", R.drawable.elliptique), db);
		ajouterExercice(new Exercice("Escalier", "Cardio", R.drawable.escalier), db);
		ajouterExercice(new Exercice("Extension jambes", "Quadriceps", R.drawable.extension_jambes), db);
		ajouterExercice(new Exercice("Flexion", "Biceps", R.drawable.flexion), db);
		ajouterExercice(new Exercice("Leg curl", "Ischio-jambiers", R.drawable.leg_curl), db);
		ajouterExercice(new Exercice("Leg press", "Ischio-jambiers", R.drawable.leg_press), db);
		ajouterExercice(new Exercice("Pompe ( push up )", "Pectoraux", R.drawable.pompe), db);
		ajouterExercice(new Exercice("Preacher curl", "Biceps", R.drawable.preacher_curl), db);
		ajouterExercice(new Exercice("Rameur", "Trap�zoidaux", R.drawable.rameur), db);
		ajouterExercice(new Exercice("Redressement", "Abdominaux", R.drawable.redressement), db);
		ajouterExercice(new Exercice("Skull crush", "Triceps", R.drawable.skull_crush), db);
		ajouterExercice(new Exercice("Soulev� de terre", "Ischio-jambiers", R.drawable.souleve_terre), db);
		ajouterExercice(new Exercice("Squat", "Quadriceps", R.drawable.squat), db);
		ajouterExercice(new Exercice("V�lo stationnaire", "Cardio", R.drawable.velo_stationnaire), db);

		db.execSQL("CREATE TABLE routine (_id INTEGER PRIMARY KEY AUTOINCREMENT, routineId NUMBER, exercice NUMBER);");

		db.execSQL("CREATE TABLE performance (exercice NUMBER PRIMARY KEY, repetition NUMBER, sequence NUMBER, duree NUMBER, commentaire TEXT, poid NUMBER);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS exercice;");
		onCreate(db);
	}

	public void ajouterExercice(Exercice e, SQLiteDatabase db){
		ContentValues cv = new ContentValues();
		cv.put("nom",  e.getNom());
		cv.put("image", e.getImage());
		cv.put("muscle", e.getMuscle());
		db.insert("exercice", null, cv);
	}

	public void ajouterRoutine(Routine r, SQLiteDatabase db){
		ContentValues cv = new ContentValues();
		cv.put("routineId",  r.getId());
		cv.put("exercice", r.getExercice());
		db.insert("routine", null, cv);
	}

	public void ajouterPerformance(Performance p, SQLiteDatabase db){
		ContentValues cv = new ContentValues();
		cv.put("repetition",  p.getRepetition());
		cv.put("sequence", p.getSequence());
		cv.put("duree", p.getDuree());
		cv.put("commentaire", p.getCommentaire());
		cv.put("poid", p.getPoid());
		cv.put("exercice", p.getExercice());
		// Comme ca, si il est deja la, on le change, sinon, on l'ajoute.
		db.insertWithOnConflict("performance", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
	}
}
