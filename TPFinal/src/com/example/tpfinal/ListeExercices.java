package com.example.tpfinal;

import java.util.ArrayList;
import java.util.Hashtable;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ListeExercices extends Activity {
	private ListView liste;
	private Operation oper;
	private Bundle intention;
	private ArrayList<Hashtable<String,String>> exos;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_liste_exercices);
		
		liste = (ListView)findViewById(R.id.exoliste);
		
		oper = new Operation(this);
		
		oper.ouvreBD();
		exos = oper.trouveExo();
		SimpleAdapter soa = new SimpleAdapter(this, exos, R.layout.exolayout,
							new String[]{"nom", "muscle", "image"}, 
							new int[]{R.id.textenomexo, R.id.textmuscle, R.id.exoimage});
		oper.fermeBD();
		
		liste.setAdapter(soa);
		Ecouteur ec = new Ecouteur();
		liste.setOnItemClickListener(ec);
		intention = getIntent().getExtras();
	}
	
	protected class Ecouteur implements OnItemClickListener{
		public void onItemClick(AdapterView<?> av, View v, int id,
				long autre) {
			// Si on le veut pour l'interactivite
			if(intention.get("INTERACTIVE").equals(1)){
				Intent data = new Intent();
				// On passe le nom de l'exercice aussi
				data.putExtra("exercice", (String)exos.get((int)autre).get("nom"));
				setResult(Activity.RESULT_OK, data);
				finish();
			}else{
				Intent i = new Intent(ListeExercices.this, Resultat.class);
				// On passe l'id de l'exercice aussi
				i.putExtra("exercice", (String)exos.get((int)autre).get("_id"));
				startActivity(i);
			}
		}
	}
}
