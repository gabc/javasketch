package com.example.tpfinal;

public class Routine {
	private int id;
	private int exercice;
	
	public Routine(int id, int excercice){
		this.id = id;
		this.exercice = excercice;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getExercice() {
		return exercice;
	}

	public void setExercice(int exercice) {
		this.exercice = exercice;
	}
}
